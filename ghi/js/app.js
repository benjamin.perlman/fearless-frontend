//    <div class="h-50 d-inline-block" style="width: 120px; background-color: rgba(0,0,255,.1)">
//style="width: 100px; 
function createCard(title, description, pictureUrl, starts, ends,location) {
    return `
    <div class=" d-inline-block" style="max-width: 33%;">

    <div class="d-grid gap-5 shadow-lg p-3 mb-5 bg-body rounded">


      <div class="card">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${title}</h5>
          <h6 class="card-subtitle">${location}</h6>

          <p class="card-text">${description}</p>
    
      </div>
      <div class="card-footer">
      <small class="text-muted"> ${starts} - ${ends}</small>
    </div>
 </div>
     
 </div>
     
    `;
  }
  

  function error() {

    return `
    <div class="alert alert-primary d-flex align-items-center" role="alert">
  <svg class="bi flex-shrink-0 me-2" width="24" height="24" role="img" aria-label="Info:"><use xlink:href="#info-fill"/></svg>
  <div>
Danger!
  </div>
</div>
`;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad
        const err = error();
        const er = document.querySelector('.err');
        er.innerHTML += err;
         throw new Error("Response not ok"); 

    } else {
        const data = await response.json();
        for (let conference of data.conferences) {
          //  console.log(conference)
            const detailUrl = `http://localhost:8000${conference.href}`;
            const detailResponse = await fetch(detailUrl);
            if (detailResponse.ok) {
              const details = await detailResponse.json();
              const title = details.conference.name;
              const description = details.conference.description;
              const starts = new Date(details.conference.starts).toLocaleDateString();
              const ends = new Date( details.conference.ends).toLocaleDateString();
              const location = details.conference.location.name;

              const pictureUrl = details.conference.location.picture_url;
              const html = createCard(title, description, pictureUrl, starts, ends, location);
              console.log(details);
              const column = document.querySelector('.col');
              column.innerHTML += html;
             
             
            }
    
        }
  
      }
    } catch (e) {
      // Figure out what to do if an error is raised
      const err = error();
      const er = document.querySelector('.err');
      er.innerHTML += err;
    error => console.error('error', error)

    }
  
  });