// The value of the options for the locations select tag should be the value of the "id" property of the location object. (This is comparable to what you did on the last page with setting the value of the option to the "abbreviation" property.)
// Notice that the JSON expects a numeric identifier for the location. You'll need to change the location list API to include the id property of locations.
// Make sure you POST to the correct API URL.
// Clear the data from the form when it succeeds.

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/locations/';
  
    const response = await fetch(url);
  
    if (response.ok) {
      const data = await response.json();
      console.log(data)
      console.log("!!")
  
      const selectTag = document.getElementById('location');

        for (let s of data.locations) {
            console.log("rr")
            console.log(s)
           
            const newDiv = document.createElement("option");
            newDiv.value= s.id;
            newDiv.innerHTML= s.id;
            selectTag.appendChild(newDiv);

    
}
    }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event => {
      event.preventDefault();
      //console.log('need to submit the form data');

    
      const formData = new FormData(formTag);
     
      const json = JSON.stringify(Object.fromEntries(formData));
   
 
      const locationUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: "post",
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };
      const response = await fetch(locationUrl, fetchConfig);
      if (response.ok) {
        formTag.reset();
        const newLocation = await response.json();
        console.log(newLocation);
      
      
    }
}); 


});
     
//});


  